using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
	RaycastHit[] hitObjects;
	Ray ray;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// Fire when the mouse button is clicked. I tried using the keyboard button Fire1, which is the Ctrl key by default, but it doesn't work well on a Mac because Ctrl-Up Arrow is a Mac way of bringing up Expose or Dashboard. Using the mouse button works better.

		/*
		if (Input.GetButtonDown("Fire1")) {
			//Debug.Log("Bullet launched");
			Fire();
		}
		*/
		
		if (Input.GetMouseButtonDown(0)) {
			//Debug.Log("Bullet launched");
			Fire();
		}
		
	}
	
	void Fire () {
		//hitObject = new RaycastHit();
		
		// Set the ray to the center of the screen.
		Vector3 rayVector;
		rayVector.x = Screen.width / 2;
		rayVector.y = Screen.height / 2;
		rayVector.z = 0.0f;
		
		ray = camera.ScreenPointToRay(rayVector);
		//ray = camera.ScreenPointToRay(Input.mousePosition);
		float rayLength = 100.0f;
		
		hitObjects = Physics.RaycastAll(ray, rayLength);
		
		for (int i = 0; i < hitObjects.Length; i++) {
			if (hitObjects[i].collider.gameObject.tag == "Onion") {
				Onion hitOnion = hitObjects[i].collider.gameObject.GetComponent("Onion") as Onion;
				hitOnion.Freeze();
				
				// Start the clock.
				GameObject gameClockObject = GameObject.Find("ClockLabel");
				Clock gameClock = gameClockObject.GetComponent("Clock") as Clock;
				if (!gameClock.Running) {
					gameClock.StartRunning();
				}
			}
		}
						
	}
}
