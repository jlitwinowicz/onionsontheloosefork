using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour {
	public Texture2D crosshairTexture;
	Rect position;
	static bool originalOn = true;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		// Setting the position of the crosshairs in the Update function allows us to deal with resizing the window.
		float left = (Screen.width - crosshairTexture.width) / 2;
		float top = (Screen.height - crosshairTexture.height) / 2;
		float width = crosshairTexture.width;
		float height = crosshairTexture.height;
		position = new Rect(left, top, width, height);
	}
	
	void OnGUI () {
		if (originalOn == true) {
			GUI.DrawTexture(position, crosshairTexture);
		}
	}
}
