using UnityEngine;
using System.Collections;

public class DropPoint : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other){
		// For now, stop the clock if it's running.
		GameObject gameClockObject = GameObject.Find("ClockLabel");
		Clock gameClock = gameClockObject.GetComponent("Clock") as Clock;
		if (gameClock.Running) {
			gameClock.Stop();
		}
	}
	
}
