using UnityEngine;
using System.Collections;

public class Clock : MonoBehaviour {
	public Font clockFont;
	float startTime;
	float elapsedTime;
	string timeString;

	// Accessors
	public bool Running {
		get; 
		set;
	}
	
	// Use this for initialization
	void Start () {
		startTime = Time.time;
		elapsedTime = 0.0f;
		timeString = "0:00";
		Running = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Running) {
			elapsedTime = Time.time - startTime;
			ShowTime();
		}
	}
	
	public void OnGUI () {
		GUIStyle style = new GUIStyle();
		Rect labelRect = new Rect (50, 50, 100, 50);
		style.font = clockFont;
		style.fontSize = 24;
		//style.font.material.color = Color.white;
		GUI.Label(labelRect, timeString, style);
	}
	
	public void ShowTime () {
		int minutes;
		int seconds;
		int elapsedTimeAsInt;
		
		elapsedTimeAsInt = (int)elapsedTime;
		minutes = elapsedTimeAsInt / 60;
		seconds = elapsedTimeAsInt % 60;
		
		timeString = minutes.ToString() + ":";
		// The D2 says to print the seconds as two digits so it looks right if the number of seconds is less than 10.
		timeString += seconds.ToString("D2");
		//Debug.Log("Elapsed Time: " + timeString);
		
	}
	
	public void StartRunning () {
		startTime = Time.time;
		Running = true;
	}
	
	public void Stop () {
		elapsedTime = 0.0f;
		Running = false;	
	}	
}
