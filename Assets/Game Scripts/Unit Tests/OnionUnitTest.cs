using UnityEngine;
using System.Collections;
using SharpUnit;

public class OnionUnitTest : TestCase {

	[UnitTest]
	public void TestThatPasses() {
		int x = 2;
		int y = 2;
		Assert.Equal(x, y, "X and Y are not equal. X = " + x + " , Y = " + y);	
	}

}
