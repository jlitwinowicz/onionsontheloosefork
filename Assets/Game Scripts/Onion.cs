using UnityEngine;
using System.Collections;

public class Onion : MonoBehaviour {
		
	// Accessors
	public float Speed {
		get; 
		set;
	}
	
	// Use this for initialization
	void Start () {
		Speed = 3.0f;
	}
	
	// Update is called once per frame
	void Update () {
		MoveOnion();
	}
	
	void MoveOnion () {
		CharacterController onionController;
		onionController = gameObject.GetComponent<CharacterController>();
		Vector3 direction = Vector3.forward;
		onionController.SimpleMove(Speed * direction);
	}
	
	public void Freeze(){
		Speed = 0.0f;
	}
		
}
